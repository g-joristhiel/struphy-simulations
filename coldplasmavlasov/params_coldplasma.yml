grid :
    Nel      : [2, 2, 256] # number of grid cells, >p
    p        : [1, 1, 3]  # spline degree
    spl_kind : [True, True, True] # spline type: True=periodic, False=clamped
    bc       : [[null, null], [null, null], [null, null]] # boundary conditions for N-splines (homogeneous Dirichlet='d')
    dims_mask : [True, True, True] # True if the dimension is to be used in the mpi domain decomposition (=default for each dimension).
    nq_el    : [2, 2, 4] # quadrature points per grid cell
    nq_pr    : [2, 2, 4] # quadrature points per histopolation cell (for commuting projectors)
    polar_ck : -1 # C^k smoothness at polar singularity at eta_1=0 (default: -1 --> standard tensor product, 1 : polar splines)

units : # units not stated here can be viewed via "struphy units -h"
    x      : 1. # length scale unit in m
    B      : 0.0017045090240360465 # magnetic field unit in T 0.003409018048072093
    n      : 0.00000028239587192945406 # number density unit in 10^20 m^(-3)

time :
    dt         : 0.05 # time step
    Tend       : 100 # simulation time interval is [0, Tend]
    split_algo : LieTrotter # LieTrotter | Strang

geometry :
    type : Cuboid # mapping F (possible types seen below)
    Cuboid :
        l1 : 0. # start of interval in eta1
        r1 : 1. # end of interval in eta1, r1>l1
        l2 : 0. # start of interval in eta2
        r2 : 1. # end of interval in eta2, r2>l2
        l3 : 0. # start of interval in eta3
        r3 : 50. # end of interval in eta3, r3>l3

mhd_equilibrium :
    type : HomogenSlab
    HomogenSlab :
        B0x  : 1.0 # magnetic field in x
        B0y  : 0. # magnetic field in y
        B0z  : 1.0 # magnetic field in z
        beta : 0. # plasma beta = 2*p*mu_0/B^2
        n0   : 1. # number density

em_fields :
    init :
        type : noise
        noise :
            comps :
                e1 : [True, False, False]   # components to be initialized (for scalar fields: no list)
                b2 : [False, False, False]   # components to be initialized (for scalar fields: no list)
            variation_in : e3 # noise variation (logical space): e1, e2, e3 (1d), e1e2, e1e3, e2e3 (2d), e1e2e3 (3d)
            amp : 1.0 # noise amplitude

fluid :
    electrons :
        phys_params:
            A : 0.0005446170214876324  # mass number in units of proton mass
            Z : -1 # signed charge number in units of elementary charge
        init :
            type : noise # initialization
            noise :
                comps :
                    j1 : [False, False, False] # components to be initialized (for scalar fields: no list)
                variation_in : e3 # noise variation (logical space): e1, e2, e3 (1d), e1e2, e1e3, e2e3 (2d), e1e2e3 (3d)
                amp : 0.1 # noise amplitude

solvers :
    solver_maxwell :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False

    solver_ohmcold :
        type : PConjugateGradient
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False

    solver_jxbcold :
        type : PBiConjugateGradientStab
        pc : MassMatrixPreconditioner # null or name of preconditioner class
        tol : 1.e-10
        maxiter : 3000
        info : False
        verbose : False
