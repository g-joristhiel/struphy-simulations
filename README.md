# Struphy simulations

This repository contains paramater files (`.yml`) for **published simulations** with the open-source Plasma Physics library [Struphy](https://gitlab.mpcdf.mpg.de/struphy/struphy).
